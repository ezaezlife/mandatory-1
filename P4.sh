a#!/bin/bash
function getiprange {
a=$(echo $1 | awk -F'.' '{print $1,$2,$3}' OFS='.');
a=$(echo $a.$i)
ping $a -c 1;
printf "\n"
}

printf "Enter an ipv4:";
read ipv4;

i=0;
while [ $i -le 255 ] 
do
echo $(getiprange $ipv4)
i=$(( $i + 1 ))
done