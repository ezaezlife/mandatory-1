function getiprange 
{
a=$(echo $1 | awk -F'.' '{print $1,$2,$3}' OFS='.');
a=$(echo $a.$i)
ips+=($a)
ping -q -n -w 1 -c 1 $a | awk -v host=$a '/packet loss/ {if ($6 == "0%") print host}'
#fping $a -a -c 1 -t500;
}
printf "This will print all IP addresses from the range that replies \n"
printf "Enter an ipv4:";
read ipv4;
i=0;
while [ $i -le 255 ] 
do
echo $(getiprange $ipv4)
i=$(( $i + 1 ))
done